# woodpecker-ci

Woodpecker is a community fork of the Drone CI system.

## Server Configuration

### User Registration

Registration is closed by default. While disabled an administrator needs to add new users manually (exp. woodpecker-cli user add).

### Server Configuration Options

Server congifuration options are stored in `/app/data/server_config`.
Make sure you have set it up to make woodpecker-ci working properly.

Please see [server-config](https://woodpecker-ci.org/docs/administration/server-config) for all available options.

[GitHub config](https://woodpecker-ci.org/docs/administration/vcs/github#configuration)

[Gitea config](https://woodpecker-ci.org/docs/administration/vcs/gitea#configuration)

[GitLab config](https://woodpecker-ci.org/docs/administration/vcs/gitlab#configuration)


