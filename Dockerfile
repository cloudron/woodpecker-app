FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=woodpecker-ci/woodpecker versioning=semver extractVersion=^v(?<version>.+)$
ARG WOODPECKER_VERSION=3.3.0

RUN wget https://github.com/woodpecker-ci/woodpecker/releases/download/v${WOODPECKER_VERSION}/woodpecker-server_linux_amd64.tar.gz -O - | tar -xz -C /app/code/

RUN wget https://github.com/woodpecker-ci/woodpecker/releases/download/v${WOODPECKER_VERSION}/woodpecker-cli_linux_amd64.tar.gz -O - | tar -xz -C /app/code/

# Add nginx config to publish a gRPC Service with TLS Encryption
RUN rm /etc/nginx/sites-enabled/* && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    ln -sf /run/woodpecker/grpc.conf /etc/nginx/sites-enabled/grpc.conf
COPY nginx/grpc.conf /app/pkg/grpc-nginx.conf
COPY nginx/readonlyrootfs.conf nginx/errors.grpc_conf /etc/nginx/conf.d/

# Add supervisor configs
COPY supervisor/ /etc/supervisor/conf.d/
RUN ln -sf /run/woodpecker/supervisord.log /var/log/supervisor/supervisord.log

COPY env.sh.template start.sh /app/code/

CMD [ "/app/code/start.sh" ]
