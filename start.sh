#!/bin/bash

set -eu

mkdir -p /run/woodpecker

if [[ ! -f /app/data/env.sh ]]; then
    echo "=> First run"
    sed -e "s/WOODPECKER_AGENT_SECRET=.*/WOODPECKER_AGENT_SECRET=$(openssl rand -hex 32)/" /app/code/env.sh.template > /app/data/env.sh
fi

source /app/data/env.sh

export WOODPECKER_HOST="${CLOUDRON_APP_ORIGIN}"
export WOODPECKER_GRPC_ADDR="localhost:9000"
export WOODPECKER_DATABASE_DRIVER=mysql
export WOODPECKER_DATABASE_DATASOURCE="${CLOUDRON_MYSQL_USERNAME}:${CLOUDRON_MYSQL_PASSWORD}@tcp(${CLOUDRON_MYSQL_HOST}:${CLOUDRON_MYSQL_PORT})/${CLOUDRON_MYSQL_DATABASE}?parseTime=true"

echo "==> Updating nginx config"
sed -e "s,GRPC_PORT,${GRPC_PORT},g" /app/pkg/grpc-nginx.conf > /run/woodpecker/grpc.conf

# ensure that data directory is owned by 'cloudron' user
chown -R cloudron:cloudron /app/data

echo "=> Starting the Woodpecker Server"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Woodpecker
