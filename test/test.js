#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD || !process.env.EMAIL) {
    console.log('USERNAME, PASSWORD and EMAIL env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const REPO_NAME = 'testrepo';

    const LOCATION = process.env.LOCATION || 'test';
    const GITEA_LOCATION = LOCATION + '-companion';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;

    let browser, app, giteaApp;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo(appLocation) {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        const app = inspect.apps.filter(function (a) { return a.location === appLocation; })[0];
        expect(app).to.be.an('object');
        return app;
    }

    async function login() {
        await browser.get(`https://${app.fqdn}`);
        await browser.executeScript('localStorage.clear();');
        await browser.manage().deleteAllCookies();

        // Woodpecker
        await browser.get(`https://${app.fqdn}`);
        await browser.wait(until.elementLocated(By.xpath('//button[text()[contains(., "Login")]]')), TIMEOUT);
        await browser.findElement(By.xpath('//button[text()[contains(., "Login")]]')).click();
        await browser.sleep(3000);

        // Gitea
        await browser.findElements(By.xpath('//h3[contains(text(), "Authorize")]'));
        await browser.findElement(By.id('authorize-app')).click();
        await browser.sleep(3000);

        // Woodpecker
        await browser.wait(until.elementLocated(By.xpath('//span[contains(text(), "Add repository")]')), TIMEOUT);

    }

    async function getMainPage() {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.xpath('//h1[contains(text(), "Welcome to Woodpecker")]')), TIMEOUT);
        await browser.sleep(2000);
    }

    async function addRepository() {
        await browser.get('https://' + app.fqdn);
        await browser.sleep(5000);
        await browser.wait(until.elementLocated(By.xpath('//span[contains(text(), "Add repository")]')), TIMEOUT);
        await browser.findElement(By.xpath('//*/span[contains(text(), "Add repository")]/..')).click();
        await browser.sleep(5000);

        await browser.wait(until.elementLocated(By.xpath('//span[contains(text(), "Enable")]')), TIMEOUT);
        await browser.findElement(By.xpath('(//button/span[contains(text(), "Enable")])[1]/..')).click();
        await browser.sleep(5000);
        await browser.get('https://' + app.fqdn);
    }

    async function checkRepository() {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.xpath(`//div[contains(., "${REPO_NAME}")]`)), TIMEOUT);
    }

    async function giteaLogin() {
        await browser.get(`https://${giteaApp.fqdn}/user/login`);
        await browser.findElement(By.id('user_name')).sendKeys('root');
        await browser.findElement(By.id('password')).sendKeys('changeme');
        await browser.findElement(By.xpath('//form[@action="/user/login"]//button')).click();
        await browser.wait(until.elementLocated(By.xpath('//img[contains(@class, "avatar")]')), TIMEOUT);
    }

    async function giteaCreateRepo() {
        await browser.get(`https://${giteaApp.fqdn}/repo/create`);
        await browser.wait(until.elementLocated(By.id('repo_name')), TIMEOUT);
        await browser.findElement(By.id('repo_name')).sendKeys(REPO_NAME);
        const button = browser.findElement(By.xpath('//button[contains(text(), "Create Repository")]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await browser.findElement(By.id('auto-init')).click();
        await browser.findElement(By.xpath('//button[contains(text(), "Create Repository")]')).click();

        await browser.wait(function () {
            return browser.getCurrentUrl().then(function (url) {
                return url === `https://${giteaApp.fqdn}/root/${REPO_NAME}`;
            });
        }, TIMEOUT);
    }

    async function addGiteaOauthClient() {
        await browser.get(`https://${giteaApp.fqdn}/user/settings/applications`);

        await browser.findElement(By.id('application-name')).sendKeys(app.fqdn);
        await browser.findElement(By.id('redirect-uris')).sendKeys(`https://${app.fqdn}/authorize`);

        const button = browser.findElement(By.xpath('//button[contains(text(), "Create Application")]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await button.click();

        const clientId = await browser.findElement(By.id('client-id')).getAttribute('value');
        const clientSecret = await browser.findElement(By.id('client-secret')).getAttribute('value');

        await browser.findElement(By.xpath('//button[contains(text(), "Save")]')).click();

        if (clientId === undefined || clientId === null || clientId == '') throw new Error('ClientID not found!');
        if (clientSecret === undefined || clientSecret === null || clientSecret == '') throw new Error('Client Secret not found!');

        console.log(`clientId: ${clientId} clientSecret: ${clientSecret}`);
        return [clientId, clientSecret];
    }

    async function configureAuth() {
        const [clientId, clientSecret] = await addGiteaOauthClient(app.fqdn);
        fs.writeFileSync('/tmp/env.sh', `#!/bin/bash\nexport WOODPECKER_OPEN=true\nexport WOODPECKER_GITEA=true\nexport WOODPECKER_GITEA_URL=https://${giteaApp.fqdn}\n`
            + `export WOODPECKER_GITEA_CLIENT="${clientId}"\nexport WOODPECKER_GITEA_SECRET="${clientSecret}"\n`);
        execSync(`cloudron push --app ${app.fqdn} /tmp/env.sh /app/data/env.sh`, EXEC_ARGS);
        fs.rmSync('/tmp/env.sh');
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('install Gitea', function () { execSync(`cloudron install --appstore-id io.gitea.cloudronapp --location ${GITEA_LOCATION}`, EXEC_ARGS); });
    it('can get Gitea app information', () => { giteaApp = getAppInfo(GITEA_LOCATION); });
    it('can Gitea login', giteaLogin);
    it('can create Gitea repo', giteaCreateRepo);

    it('install app', function () { execSync(`cloudron install --no-wait --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', () => { app = getAppInfo(LOCATION); });

    it('can configure auth', configureAuth);

    it('can get the main page', getMainPage);
    it('can login', login);
    it('can add repository', addRepository);
    it('has repository added', checkRepository);

    xit('check grpc port');

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('has repository added', checkRepository);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        app = getAppInfo(LOCATION);
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('has repository added', checkRepository);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        app = getAppInfo(`${LOCATION}2`);
    });

    it('can configure auth', configureAuth);
    it('can get the main page', getMainPage);
    it('can login', login);
    it('has repository added', checkRepository);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    it('can install app (for update)', function () { execSync(`cloudron install --appstore-id org.woodpecker_ci.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', () => { app = getAppInfo(LOCATION); });

    it('can configure auth', configureAuth);

    it('can get the main page', getMainPage);
    it('can login', login);
    it('can add repository', addRepository);
    it('has repository added', checkRepository);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can get app information', () => { app = getAppInfo(LOCATION); });

    it('has repository added', checkRepository);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    it('uninstall Gitea', function () {
        execSync(`cloudron uninstall --app ${giteaApp.id}`, EXEC_ARGS);
    });
});

