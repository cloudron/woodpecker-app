## About

Woodpecker is a simple CI engine with great extensibility. It runs your pipelines inside Docker containers, so if you are already using them in your daily workflow, you'll love Woodpecker for sure.

## Features

* OpenSource and free - Woodpecker is and always will be totally free. As Woodpecker's source code is open-source you can contribute to help evolving the project.

* Based on docker containers - Woodpecker uses docker containers to execute pipeline steps. If you need more than a normal docker image, you can create plugins to extend the pipeline features. How do plugins work?

* Multi pipelines - Woodpecker allows you to easily create multiple pipelines for your project. They can even depend on each other. Check out the docs

