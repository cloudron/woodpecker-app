To use Woodpecker:

* Configure [authentication](https://docs.cloudron.io/apps/woodpecker#authentication) using credentials from your existing Version Control System (Github/Gitea/Gogs/GitLab etc). Note that you cannot login without this configuration step.

* Setup one or more [agents](https://docs.cloudron.io/apps/woodpecker#agent) outside the Cloudron VM.

